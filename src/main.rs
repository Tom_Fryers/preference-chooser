use itertools::Itertools;
use std::cmp::PartialEq;
use std::collections::HashMap;
use std::fs;
use std::io;
use std::iter::FromIterator;
const AVOID_REPEAT_DECAY: f64 = 0.8;
const FAIRNESS_DECAY: f64 = 0.9;
const REPEAT_POWER: f64 = 2.0;
const AVOID_REPEAT_WEIGHT: f64 = 0.6;
const YEARNING_WEIGHT: f64 = 0.3;
fn read_csv(filename: &str) -> Result<Vec<Vec<String>>, std::io::Error> {
    let file_contents = fs::read_to_string(filename)?;
    let mut result: Vec<Vec<String>> = Vec::new();
    for line in file_contents.lines() {
        let row: Vec<_> = line.split(',').map(String::from).collect();
        result.push(row.clone());
    }
    Ok(result)
}
fn _write_csv(filename: &str, data: Vec<Vec<String>>) -> Result<(), std::io::Error> {
    let mut string = String::new();
    for row in data {
        string.push_str(&row.join(","));
        string.push('\n');
    }
    fs::write(filename, string)?;
    Ok(())
}
#[derive(Debug)]
struct Person {
    name: String,
    rankings: HashMap<String, i32>,
    history: Vec<Vec<String>>,
}
impl Person {
    fn get_scaled_rank(&self, given: &str) -> f64 {
        self.rankings[given] as f64 / self.rankings.len() as f64
    }
    fn get_likes(&self, given: &str) -> f64 {
        let base_rate = -self.get_scaled_rank(given);
        let total_had = self.history.iter().map(|x| x.len()).sum::<usize>() as i32;
        let mut had_so_far = 0i32;
        let mut recentness = 0.0;
        let mut yearning = 0.0;
        for had in &self.history {
            if had.contains(&String::from(given)) {
                recentness +=
                    AVOID_REPEAT_WEIGHT * AVOID_REPEAT_DECAY.powi((total_had - had_so_far) as i32);
                yearning *= 0.1;
            } else {
                yearning = (1.0 - self.get_scaled_rank(given) / self.rankings.len() as f64).powi(3)
                    * YEARNING_WEIGHT;
            }
            had_so_far += had.len() as i32;
        }
        // println!("{:#} is given {:#}", self.name, given);
        // println!("Base rate: {:#}", base_rate);
        // println!("Recentness: {:#}", recentness);
        // println!("Yearning: {:#}", yearning);
        base_rate as f64 - recentness as f64 + yearning
    }
    fn get_happiness(&self, given: &[&String]) -> f64 {
        let mut total = 0.0;
        let mut had = HashMap::new();

        for g in given {
            let had_of_this = had.entry(g).or_insert(0);
            if *had_of_this > 0 {
                total -= 1.5
                    * (*had_of_this as f64).powf(REPEAT_POWER)
                    * (1.0 + self.get_scaled_rank(g));
            }
            *had_of_this += 1;
            total += self.get_likes(g);
        }
        total
    }
    fn happiness_at_point(&self, point: usize) -> f64 {
        let fake_person = Person {
            name: String::from(""),
            rankings: self.rankings.clone(),
            history: self.history[..point].to_vec(),
        };
        fake_person.get_happiness(&self.history[point].iter().collect::<Vec<_>>())
    }
    fn happiness_so_far(&self) -> f64 {
        (0..self.history.len())
            .map(|x| self.happiness_at_point(x))
            .zip((0..self.history.len()).map(|x| FAIRNESS_DECAY.powi(x as i32)))
            .map(|(x, y)| x * y)
            .sum()
    }
}
fn remove_from_vec<T: PartialEq>(vec: &mut Vec<T>, removes: &[&T]) {
    for &r in removes {
        vec.remove(vec.iter().position(|x| *x == *r).unwrap());
    }
}
struct PossibilityList<'a, T: PartialEq + Clone> {
    items: &'a [T],
    split_into: u32,
    one_part: itertools::structs::Dedup<itertools::structs::Combinations<std::slice::Iter<'a, T>>>,
    possibilities: Vec<Vec<Vec<T>>>,
}
impl<T: PartialEq + Clone> PossibilityList<'_, T> {
    fn new(items: &[T], split_into: u32) -> PossibilityList<T> {
        let one_part = items
            .iter()
            .combinations(items.len() / split_into as usize)
            .dedup();
        let possibilities = Vec::new();
        PossibilityList {
            items,
            split_into,
            one_part,
            possibilities,
        }
    }
}
impl<'a, T: PartialEq + Clone> Iterator for PossibilityList<'a, T> {
    type Item = Vec<Vec<T>>;
    fn next(&mut self) -> Option<Vec<Vec<T>>> {
        if self.possibilities.is_empty() {
            if self.items.is_empty() {
                return None;
            }
            let p = self.one_part.next()?;
            if self.split_into == 1 {
                self.possibilities = Vec::new();
                return Some(vec![self.items.to_owned()]);
            }
            let mut remaining = self.items.to_owned();
            remove_from_vec(&mut remaining, &p);
            self.possibilities = PossibilityList::new(&remaining, self.split_into - 1).collect();
            for sp in &mut self.possibilities {
                sp.push(p.iter().map(|x| (**x).clone()).collect());
            }
        }
        // panic!("");
        Some(self.possibilities.pop().unwrap())
    }
}
fn weight_from_happiness(hap: f64) -> f64 {
    (-hap * 0.5).exp()
}
fn rate_possibility(people: &[Person], allocations: &[Vec<&String>], unfairness: &[f64]) -> f64 {
    let haps: Vec<_> = people
        .iter()
        .zip(allocations)
        .map(|(person, allocation)| person.get_happiness(allocation))
        .collect();
    let unfairness: Vec<_> = unfairness
        .iter()
        .zip(&haps)
        .map(|(unfair, hap)| unfair + hap)
        .collect();
    let average_unfairness = unfairness.iter().sum::<f64>() / unfairness.len() as f64;
    haps.iter()
        .zip(unfairness)
        .map(|(hap, previous)| hap * weight_from_happiness(previous - average_unfairness))
        .sum()
}
fn get_unfairness(people: &[Person]) -> Vec<f64> {
    people.iter().map(Person::happiness_so_far).collect()
}
fn get_best_possibility<'a>(
    people: &[Person],
    options: &[&'a String],
) -> HashMap<String, Vec<&'a String>> {
    let mut current_best = f64::NEG_INFINITY;
    let mut current_choice = Vec::new();
    let unfairness = get_unfairness(people);
    println!(
        "Current unfairness: {:#?}",
        people
            .iter()
            .map(|x| x.name.clone())
            .zip(unfairness.iter())
            .collect::<Vec<_>>()
    );
    for p in PossibilityList::new(options, people.len() as u32) {
        let score = rate_possibility(people, &p, &unfairness);
        if score > current_best {
            current_best = score;
            current_choice = p.to_vec();
        }
    }
    let names = people.iter().map(|x| x.name.clone());
    names.zip(current_choice.into_iter()).collect()
}

fn main() {
    let data = read_csv("ranks.csv").unwrap();
    let history = read_csv("history.csv").unwrap();
    let mut people = Vec::new();
    for (person_index, person_name) in data[0].iter().enumerate() {
        let person_history_index = history[0].iter().position(|x| x == person_name).unwrap();
        let mut person_history: Vec<Vec<String>> = Vec::new();
        for thing in history[1..].iter() {
            person_history.push(Vec::from_iter(
                thing[person_history_index].split('+').map(String::from),
            ));
        }
        let mut rankings = HashMap::new();
        for (index, thing) in data[1..].iter().enumerate() {
            rankings.insert(String::from(&thing[person_index]), index as i32);
        }
        people.push(Person {
            name: person_name.to_string(),
            rankings,
            history: person_history,
        });
    }
    let available: Vec<String> = people[0].rankings.keys().map(|s| (*s).clone()).collect();
    println!("{:?}", available);
    // println!(
    //     "{:#?}",
    //     get_best_possibility(&people, &(available.iter().collect()))
    // );
    let mut options = Vec::new();
    let input = io::stdin();
    loop {
        let mut input_line = String::new();
        input.read_line(&mut input_line).unwrap();
        let input_line = String::from(input_line.trim());
        if available.contains(&input_line) {
            options.push(available.iter().find(|s| **s == input_line).unwrap());
        } else if input_line.is_empty() {
            break;
        } else {
            eprintln!("error: not a choice")
        }
    }
    println!("{:#?}", get_best_possibility(&people, &options));
}
