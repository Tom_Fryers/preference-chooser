# Preference Chooser

Suppose a group of _n_ people receive, to share, a group of _kn_ things
between them regularly. They have different views as to the relative
value of the things, and it is also preferable to get a variety of
things than the same repeatedly. This program can produce a sequence of
relatively fair assignments.

To use it, create `ranks.csv` in this directory, which is a CSV file,
with one column per person. The heading row gives the person’s name. The
rest of the rows give their ranking of the available options, with the
best at the top.

Then, create a `history.csv` file. The structure of this is similar, but
it instead gives a history of the things previously assigned to that
person, with those assigned each time `+`-separated.

Then run the chooser with `cargo run --release`, and enter the available
options this time, pressing return each time. When done, press return again.

## Example

Alice and Bob are sharing ink. They get four random jars of ink between
them each month. They put their preferences into `ranks.csv`.
```csv
Alice,Bob
Red,Blue
Blue,Black
Black,Red
```

They create `history.csv` with a single `Alice,Bob` row.

In the first month, they get two blue and one jar of each of red and
black. They put this in, and the program gives

```rust
{
    "Bob": [
        "Blue",
        "Black",
    ],
    "Alice": [
        "Red",
        "Blue",
    ],
}
```

so they divide the ink up this way, and enter it into `history.csv`.

```csv
Alice,Bob
Red+Blue,Blue+Black
```

This process is repeated indefinitely.
